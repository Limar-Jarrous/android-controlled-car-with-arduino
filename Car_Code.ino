int in1=8;  //  Negative pole of the first motor
int in2=7;  //  Positive pole of the first motor
int in3=5;  //  Positive pole of the second motor
int in4=4;  //  Negative pole of the second motor
char data;  //  to store the direction of movement

void setup() {
	Serial.begin(38400);  // Setting BaudRate of Bluetooth  
	pinMode( in1 , OUTPUT );
	pinMode( in2 , OUTPUT );
	pinMode( in3 , OUTPUT );
	pinMode( in4 , OUTPUT );
}

void loop() {
	if( Serial.available() ){  // Check if bluetooth is recieving data
		data = Serial.read();  // Reading data from bluetooth    
	}
	if (data=='a'){    // LEFT
		digitalWrite( in1 , HIGH);
		digitalWrite( in2 , LOW);
		digitalWrite( in3 , HIGH);
		digitalWrite( in4 , LOW );
	}
	if (data=='b'){    // BACKWARD
		digitalWrite( in1 , HIGH );
		digitalWrite( in2 , LOW );
		digitalWrite( in3 , LOW );
		digitalWrite( in4 , HIGH );
	}
	if (data=='c'){     // STOP
		digitalWrite(in1,LOW);
		digitalWrite(in2,LOW);
		digitalWrite(in3,LOW);
		digitalWrite(in4,LOW);
	}
	if (data=='d'){     // FORWARD
		digitalWrite( in1  , LOW );
		digitalWrite( in2 , HIGH );
		digitalWrite( in3 , HIGH );
		digitalWrite( in4 , LOW );
	}
	if (data=='e'){     // RIGHT
		digitalWrite( in1 , LOW( ;
		digitalWrite( in2 , HIGH );
		digitalWrite( in3 , LOW (;
		digitalWrite( in4 , HIGH );
	}
	delay(50);  // Wait for 50 ms
}
