# Android-Controlled_Car_using_Arduino
HOW TO MAKE AN ANDROID-CONTROLLED CAR USING ARDUINO AND BLUETOOTH  :

 1. Connect the Bluetooth Module to the Arduino (see Arduino-Bluetooth Connection Diagram.jpg)
 2. Connect the L298N Motor Driver to the Arduino (see Arduino-L298N Motor Connection Diagram.jpg)
 3. Connect the Arduino to your computer. Make sure the RX pin on the Arduino isn't connected or you won't be able to upload to the Arduino. 
 4. Upload the Arduino code.
 5. Connect the RX Pin on the Arduino after uploading the code.
 6. Install the app with the .apk file to your Android Smartphone.
 7. Put in the batteries for the L298N Motor Driver. When powered on, the Motor Driver's LED should glow.
 8. Open the Android App
 9. If your bluetooth is turned off, it will ask permission to turn it on.
10. After bluetooth is turned on, Connect to the "HC-05" device.
11. After a second or two, the Bluetooth Module's LED should stop blinking, indicating a connection has been made.
12. Enjoy :)

IMPORTANT_NOTE:
- Use Arduino pins which used in the code not in the images. The images are just an example of wiring.
